#How to run
Clone project:
``` 
git clone https://bitbucket.org/matifly/google_scraper.git
```
Change directory to project directory:
```
cd google_scraper
```
create fresh virtual environment and activate it:
```
python -m venv local_python_environment

source local_python_environment/bin/activate
```
Install requirements:
```
pip install -r requirements.txt 
```

Run migrations:
```
python manage.py migrate
```


Move chromedriver file from `./bin ` directory to `local_python_environment/bin` directory.


Run server
```
python manage.py runserver
```
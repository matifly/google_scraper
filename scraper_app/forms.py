from django import forms


class SearchForm(forms.Form):
    phrase = forms.CharField(label="", widget=forms.TextInput(attrs={"placeholder":"Szukaj...", "class": "phrase"}))
    time_delta = forms.IntegerField(label="Pobierz z bazy wyniki nie starsze niż", required=False, label_suffix=" [s]", widget=forms.NumberInput(attrs={"value":0, "placeholder":"Podaj czas w sekundach"}))


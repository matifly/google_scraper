from django.urls import path

from scraper_app import views

urlpatterns = [
    path('', views.search, name='search-view'),
]
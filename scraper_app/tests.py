from unittest.mock import patch, MagicMock

from django.test import TestCase

from .models import Result, Search
from .services import search_service
from .services.keywords_counter import KeywordsCounter


class SearchServiceTestCase(TestCase):

    @patch('scraper_app.models.Search.objects')
    def test_get_search_object_newer_than(self, mock_search):
        search = Search()
        mock_search.filter().first.return_value = search
        response = search_service.get_search_object_newer_than("Google Scraper", 600)
        self.assertEqual(response, search)

    @patch('scraper_app.models.Result.objects')
    @patch('scraper_app.models.Search.objects')
    @patch('scraper_app.services.search_service.KeywordsCounter')
    @patch('scraper_app.services.search_service.ScraperService')
    def test_new_search(self, mock_scraper,
                        mock_keywords_counter,
                        mock_search,
                        mock_result
                        ):
        phrase = "Google Scraper"
        ip = '127.0.0.1'
        results_count = 200
        popular_keywords = [("a", 2), ("b", 1)]
        search = Search(phrase=phrase)

        results = [{
            "title": "title 1",
            "description": "description 1",
            "link": "link",
        }]
        scraper_instance = mock_scraper.return_value
        scraper_instance.scrape.return_value = [results_count, results]

        counter_instance = mock_keywords_counter.return_value
        counter_instance.get_most_popular_keywords.return_value = popular_keywords

        mock_search.create.return_value = search
        mock_result.create.return_value = Result()

        response = search_service.new_search('Google Scraper', ip)
        scraper_instance.scrape.assert_called_with('Google Scraper')
        mock_search.create.assert_called_with(
            phrase=phrase,
            ip=ip,
            results_count=results_count,
            popular_keywords='[["a", 2], ["b", 1]]'
        )
        mock_result.create.assert_called_with(
            **results[0],
            search=search
        )
        self.assertEqual(response, search)


class KeywordsCounterTestCase(TestCase):
    def setUp(self) -> None:
        self.keywords_counter = KeywordsCounter()

    def test_get_most_popular_keywords(self):
        # with patch('scraper_app.services.keywords_counter.get_all_keywords') as mocked_keywords:
        return_value = {
            "a": 1,
            "b": 2,
            "c": 5
        }
        results = ['el1', 'el2']
        self.keywords_counter.get_all_keywords = MagicMock(return_value=return_value)

        response = self.keywords_counter.get_most_popular_keywords(results)

        self.keywords_counter.get_all_keywords.assert_called_with(results)
        self.assertEqual(response, [
            ("c", 5), ("b", 2), ("a", 1)
        ], response)

    def test_get_all_keywords(self):
        search = Search()
        result0 = {'title': "first result title", 'description': "first result description", 'search': search,
                   'link': "http://test.com"}
        result1 = {'title': "second result", 'description': "second result description", 'search': search}
        results = [result0, result1]

        results = self.keywords_counter.get_all_keywords(results)

        self.assertEqual(results, {
            "first": 2,
            "title": 1,
            "result": 4,
            "second": 2,
            "description": 2
        })

import json

from django.shortcuts import render
from ipware import get_client_ip

from .forms import SearchForm
from .services.search_service import get_search_object_newer_than, new_search


# Create your views here.

def search(request):

    ip = get_client_ip(request)[0]
    phrase = request.GET.get('phrase')

    if phrase:
        phrase = phrase.lower()
        time_delta_seconds = request.GET.get('time_delta') or 0
        form = SearchForm(request.GET)

        search = get_search_object_newer_than(phrase, time_delta_seconds)
        if not search:
            search = new_search(phrase, ip)
        popular_keywords = json.loads(search.popular_keywords)
        results = search.result_set.all()

    else:
        form = SearchForm
        search = None
        results = []
        popular_keywords = {}

    ctx = {
        'form': form,
        'phrase': phrase,
        'ip': ip,
        'results_count': search.results_count if search else '0',
        'results': results,
        'popular_keywords': popular_keywords
    }

    return render(request, 'search_template.html', ctx)



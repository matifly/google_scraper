# -*- coding: utf-8 -*-

import datetime

from django import forms


def duration_string(duration):
    # Removing the milliseconds of the duration field
    days = duration.days
    seconds = duration.seconds
    microseconds = duration.microseconds

    minutes = seconds // 60
    seconds = seconds % 60

    hours = minutes // 60
    minutes = minutes % 60

    string = '{:02d}:{:02d}:{:02d}'.format(hours, minutes, seconds)
    if days:
        string = '{} '.format(days) + string
    # if microseconds:
    #     string += '.{:06d}'.format(microseconds)

    return string


class CustomDurationField(forms.DurationField):
    def prepare_value(self, value):
        if isinstance(value, datetime.timedelta):
            return duration_string(value)
        return value

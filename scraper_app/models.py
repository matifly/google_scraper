from django.db import models


class Search(models.Model):
    phrase = models.CharField(null=False, max_length=255)
    ip = models.GenericIPAddressField(null=False)
    results_count = models.IntegerField(null=True)
    popular_keywords = models.TextField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)

class Result(models.Model):
    index = models.IntegerField()
    link = models.URLField()
    title = models.CharField(max_length=255)
    description = models.TextField()
    search = models.ForeignKey(Search, on_delete=models.CASCADE)
